

class DevelopmentConfig:
    def __init__(self):
        print("Development Class Init")
    def test(self, val=None):
        if val != None:
            print("Development Class Loaded", val)
        else:
            print("Development Class Loaded")


class TestingConfig:
    def __init__(self):
        print("Testing Class Init")
    def test(self, val=None):
        if val != None:
            print("Testing Class Loaded", val)
        else:
            print("Testing Class Loaded")


cfg_dir = {
    'development': DevelopmentConfig,
    'testing': TestingConfig
}