
import os

def show_dir():
    print("1:", __package__)
    print("2:", __file__)
    print("3:", os.path.dirname(__file__))
    print("4:", os.path.abspath(__file__))
    print("5:", os.path.dirname(os.path.abspath(__file__)))
    print("6:", os.path.abspath(os.path.dirname(__file__)))

def hello():
    print("Say, Hello!")

print("--- myapp package is imported ---")

show_dir()

hello()

if __name__ == "__main__":
    print("-----------__init__-----------")
else:
    print("main __name__ is set to: {}" .format(__name__))
